"use strict";
const arr1 = [
  {name: 'Alisa', point:  10},
  {name: 'Masha', point: 10},
  {name: 'Timur', point: 10}
];
const arr2 = [
  {task: 'create html', number: 7},
  {task: 'create css', number: 6},
  {task: 'create js', number: 3},
  {task: 'create project', number: 2}
];

let date = '2022/12/30';

function getDedlayn (arr1, arr2, date ) {
const storPoint = arr1.map(arr1 => arr1.point);
const allStorPoint = storPoint.reduce((item, current) => item + current, 0); // сумма дел, которые могут сделать работники за 1 рабочий день.
console.log(`За один рабочий день команда делает ${allStorPoint} задач`); 

const numberTasc = arr2.map(arr2 => arr2.number);
const allNumberTasc = numberTasc.reduce((item, current) => item + current, 0); // сумма дел, которые нужно успеть сделать до делайна.
console.log(`Нужно сделать до дедлайна ${allNumberTasc} задач`);

// работаем с дедлайном
// let end = '2022/12/31';
let x = new Date();
let y = new Date(date);
const diff = Math.floor((y - x) / (1000 * 60 * 60 * 24)) + 1; // сколько дней на выполнение задания
console.log(`Задания нужно выполнить за ${diff} ${num_word(diff, ['день', 'дня', 'дней'])} до ${date}`);

let daysToWork = Math.ceil(allNumberTasc / allStorPoint); // посчитаем за сколько дней работники выполнят дело
console.log(`На выполнение задач сотрудникам требуется ${daysToWork} ${num_word(daysToWork, ['день', 'дня', 'дней'])}`);

let differenceInDays = diff - daysToWork; // за сколько дней до дедлайна выполнят

let needDays = daysToWork - diff; // нужно еще столько дней, если не успевают


if (daysToWork <= diff) {
  console.log(`Задания будут успешно выполнены за ${differenceInDays} ${num_word(differenceInDays, ['день', 'дня', 'дней'])} до дедлайна`);
}

if(daysToWork > diff) { console.log(`Сотрудники не смогут выполнить задания в срок. Нужно еще ${needDays} ${num_word(needDays, ['день', 'дня', 'дней'])}.`);

}

function num_word(value, words){  
	value = Math.abs(value) % 100; 
	var num = value % 10;
	if(value > 10 && value < 20) return words[2]; // дней
	if(num > 1 && num < 5) return words[1]; // дня
	if(num == 1) return words[0];  // день
	return words[2];
}
 
 }
getDedlayn (arr1, arr2, date);
